import FeedbackItem from "./FeedbackItem";

const FeedbackList = ({ _feedback, handleDelete }) => {
  if (!_feedback || _feedback.length === 0) {
    return <p>No Feedback Yet.</p>;
  }

  return (
    <div className="feedback-list">
      {_feedback.map((item) => (
        <FeedbackItem key={item.id} _item={item} _handleDelete={handleDelete} />
      ))}
    </div>
  );
};

export default FeedbackList;