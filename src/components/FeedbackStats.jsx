import PropTypes from "prop-types";

const FeedbackStats = ({ _feedback }) => {
  // Calculate rating average
  let size = _feedback.length;
  let average =
    _feedback.reduce((accumalator, current) => {
      return accumalator + current.rating;
    }, 0) / size;

  average = average.toFixed(1).replace(/[.,]0$/, "");

  console.log(average);

  return (
    <div className="feedback-stats">
      <h4>{size <= 1 ? `${size} Review` : `${size} Reviews`} </h4>
      <h4>Average Rating: {isNaN(average) ? 0 : average}</h4>
    </div>
  );
};

FeedbackStats.propTypes = {
  _feedback: PropTypes.array.isRequired,
};

export default FeedbackStats;
