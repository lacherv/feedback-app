import PropTypes from "prop-types";
import { FaTimes } from "react-icons/fa";
import Card from "./shared/Card";

//     <Card>
//       <div className="num-display">{_item.rating}</div>
//       <button onClick={() => _handleDelete(_item.id)} className="close">
//         <FaTimes color="purple" />
//       </button>
//       <div className="text-display">{_item.text}</div>
//     </Card>


const FeedbackItem = ({ _item, _handleDelete }) => {
  return (
    <div className="card">
      <div className="num-display">{_item.rating}</div>
      <button className="close" onClick={() => _handleDelete(_item.id)}>
        <FaTimes />
      </button>
      <div className="text-display">{_item.text}</div>
    </div>
  );
};

FeedbackItem.propTypes = {
  _item: PropTypes.object.isRequired,
};

export default FeedbackItem;
